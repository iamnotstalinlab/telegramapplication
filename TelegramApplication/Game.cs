﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramApplication.DataModels;

namespace TelegramApplication {
    [Serializable]
    public class Game {
        public List<Player> Players { get; set; }
        int playersPlayed { get; set; }

        public Settings Settings { get; set; }

        

        public String Mode { get; set; }
        public string Comment { get; set; }

        public int TitleId { get; set; }
        public Chat GameChat { get; set; }

        public string Status; // empty, ready, running, registration, finished, over,

        int chIndex; //index of champion
        int idAdmin = 771039930;

        public event EventHandler<ImportantMessage> OnAction;
        public event EventHandler<ImportantMessage> OnMessage;
        public event EventHandler<MistakeMessage> OnMistake;

        public class MistakeMessage : EventArgs {
            public int Id { get; set; }
        }

        public class ImportantMessage : EventArgs {
            public string Text { get; set; }
            public int UserId { get; set; }
            public string Status { get; set; }
            public long chatId { get; set; }
        }

        System.Timers.Timer breakTimer;
        System.Timers.Timer gameTimer;

        const int breakInterval = 40 * 60 * 1000;
        //const int breakInterval = 20 * 1000;

        public void Launch() {

            Status = "run";
            Mode = CreateMode();


            breakTimer = new System.Timers.Timer();

            breakTimer.Interval = breakInterval;
            breakTimer.Elapsed += BreakTimer_Elapsed;
            breakTimer.Start();

            Players = new List<Player>();
            string text = $"Играем в {Mode}" +
                $"\n\n{Comment}";
            OnAction?.Invoke(this, new ImportantMessage { Text = text, Status ="start" });
        }

        

        private void BreakTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {
            if(Players.Count > 0) {

                ScoreSort();
                string text = $"<b>Лидеры</b> турнира:\n\n";

                for (int i =0; i <Players.Count() && i <5; i++) {
                    text += $"<b>{i + 1})</b> " +
                        $"<a href=\"https://t.me/{GameChat.Username}/{Players[i].IdOfLastMessage} \">" +
                        $"{Players[i].FirstName} {Players[i].LastName}.</a>";

                    if(Players[i].Drop != 5) {
                       text += $"\nСумма баллов: <b>{Players[i].Score}</b>\n\n";
                    } else {
                        text += $"\n<s>Сумма баллов:</s> <b>{Players[i].Score}</b>\n\n";
                    }
                }

                if (Players.Count > 5) {
                    text += "\n...";
                }
                OnMessage?.Invoke(this, new ImportantMessage { Text = text, Status = "hourElapsed" });
            }
        }

        private string CreateMode() {
            string mode ="Дартс";

            //Random random = new Random();
            //int rnd = random.Next(0, 3);
            //switch (rnd)
            //{
            //    case 0:
            //        mode = "🎯";
            //        break;
            //    case 1:
            //        mode = "🎲";
            //        break;
            //    case 2:
            //        mode = "🏀";
            //        break;
            //    case 3:
            //        mode = "🎳";
            //        break;
            //}

            //mode = "SLOT_MACHINE";
            mode = "🎰";
            return mode;
        }

        //Выполняю игровое действие

        public void ExecuteAction(Message message) {

            Run(message);
        }

        #region Game Main Part


        //Провожу действие в игру
        private void Run(Message message) {

            
            if ((message.Type == MessageType.Dice) &&
                (message.Dice.Emoji == Mode) &&
                (!message.IsForwarded)) { 

                string text ="";
                Player current;
                current = Players.Find((i) => i.Id == message.From.Id);

                if(current == null) {
                    current = new Player(message.From);
                    Players.Add(current);
                }
                if(current.Drop < Settings.MaxThrows) {
                    int score = CalculateScore(message.Dice.Value);

                    current.Score += score;
                    ++current.Drop;

                    Thread.Sleep(4000);
                    text = $"Бросок: <b>{current.Drop}</b>/{Settings.MaxThrows}" +
                        $"\nУчастник: {current.FirstName}" +
                        $"\n\nСчёт пополнился на <b>{score} балл</b>(а/ов)" +
                        $"\nОбщая сумма: <b>{current.Score} балл</b>(а/ов)";

                    if (current.Drop !=Settings.MaxThrows) {
                        current.IdOfLastMessage = message.MessageId;
                        current.DateOfLastMessage = message.Date.ToLocalTime();

                        OnMessage?.Invoke(this, new ImportantMessage { Text = text});
                    } else {
                        current.DateOfLastMessage = message.Date.ToLocalTime();
                        current.IdOfLastMessage = message.MessageId;
                        playersPlayed++;

                        text += "\n\nУчастник выполнил все броски и <b>закрыл</b> свою <b>игру</b>.";
                        text += $"\n\n <i>Полностью сыграли {playersPlayed} /{Players.Count}</i>";
                        OnMessage?.Invoke(this, new ImportantMessage { Text = text, UserId = current.Id, Status = "full" });
                    }
                }
            } else {
                if (message.From.Id != idAdmin) {
                    OnMistake?.Invoke(this, new MistakeMessage { Id = message.MessageId });
            }
        } 

        }

        public void Finish() {
            if(Players.Count != 0) {
                string title;

                ScoreSort();
                Player champion = Players[0];

                Status = "over";
                title = $"🏆🏆🏆\n<b>Победитель турнира</b>:\n" +
                    $"<a href=\"https://t.me/{GameChat.Username}/{champion.IdOfLastMessage} \">" +
                    $"{champion.FirstName} {champion.LastName}.</a>" +
                    $"\nРезультат: <b>{champion.Score}</b>" +
                    "\n\nТурнир окончен!\n" +
                    "Для получения приза <b>напишите в чат</b>: " +
                    "\nhttps://t.me/iamnotstalindiscuss";
                OnMessage?.Invoke(this, new ImportantMessage { Text = title, Status = Status, UserId = Players[chIndex].Id });

                breakTimer.Stop();
            }
        }
        #endregion


        #region Game Processes
        private int CalculateScore(int score) {

            switch (Mode) {
                case "🎯":
                    CalculateDarts(ref score);
                    break;
                case "🏀":
                    CalculateBall(ref score);
                    break;
                case "🎲":
                    CalculateCube(ref score);
                    break;
                case "🎳":
                    CalculateBowling(ref score);
                    break;
            }
            return score;
        }

        private int CalculateDarts(ref int score) {
            switch (score) {
                case 6:
                    score = 10;
                    break;
                case 5:
                    score = 9;
                    break;
                case 4:
                    score = 8;
                    break;
                case 3:
                    score = 7;
                    break;
                case 2:
                    score = 6;
                    break;
                case 1:
                    score = 0;
                    break;
            }

            return score;
        }

        private int CalculateBall(ref int score) {
            if (score > 3) {
                score = 1;
            } else {
                score = 0;
            }
            return score;
        }

        private int CalculateCube(ref int score) {
            return score;
        }

        private int CalculateBowling(ref int score){

            if (score <3){
                score--;
            }
            return score;
        }

        private void ScoreSort() {
            //Сначала сортируем по дате отправленного сообщения. (Чем раньше тем лучше)
            Players = Players.OrderBy(o => o.DateOfLastMessage).ToList();

            //Теперь сортируем по сделанным броскам. (Чем больше бросков, тем лучше)
            Players = Players.OrderByDescending(o => o.Drop).ToList();

            //И наконец сортируем по баллам. (Чем больше тем лучше)
            Players = Players.OrderByDescending(o => o.Score).ToList();
        }
        #endregion


    }
}


