﻿using System;
using Newtonsoft.Json;
using Telegram.Bot.Types;

namespace TelegramApplication {

    public class Player : User {
        public int Drop { get; set; }
        public int Score { get; set; }
        public DateTime DateOfLastMessage { get; set; }
        public int IdOfLastMessage { get; set; }

        public Player() { }
        public Player(User user) {
            this.FirstName = user.FirstName;
            this.Id = user.Id;
            this.LastName = user.LastName;
        }

    }
}


