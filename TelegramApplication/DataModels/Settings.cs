﻿using System;
namespace TelegramApplication.DataModels {
    public class Settings {

        //Время начала игры
        public DateTime StartTime { get; set; }

        // Время окончания игры
        public DateTime EndTime { get; set; }

        //Описание к игре
        public string Description { get; set; }

        //Максимальное число бросков
        public int MaxThrows { get; set; }

        //Число победителей
        public int NumberOfWinners { get; set; }

        //Включён ли обратный режим победителей
        public Boolean isReverseMode { get; set; }

        //Имя телеграм-канала
        public string ChannelName { get; set; }

        //Имя игрового чата
        public string ChatName { get; set; }

        //Номер сообщения о начале турнира
        public int TitleMessageId { get; set; }

        //Режим игры (Во что играем: дартс, мяч и тд)
        public string ModeGame { get; set; }

        public Settings() {
        }
    }
}
