﻿using System;
using System.Collections.Generic;

namespace TelegramApplication.DataModels {
    public class Poll {

        Dictionary<long, int> vUsers;
        List<int> votes;

        public Poll(int options) {
            votes = new List<int>(options);
            vUsers = new Dictionary<long, int>();
        }


        //Засчитываю голоса пользователей
        public void SetUserVote(long idUser, int vote) {

            //Если пользователь оставил голос
            if(vote >0) {

                if(!vUsers.ContainsKey(idUser)) {
                    vUsers.Add(idUser, vote);
                } else {
                    vUsers[idUser] = vote;
                }

                votes[vote]++;

            } else {    //Если пользователь снял свой голос

                if(vUsers.ContainsKey(idUser)) {
                    votes[vUsers[idUser]]--;
                    vUsers[idUser] = -1;
                }
            }
        }

        //Очистить голоса
        public void Clear() {
            votes.Clear();
            vUsers.Clear();
        }



    }
}
