﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.Payments;
using Telegram.Bot.Types.ReplyMarkups;
using static TelegramApplication.Game;

namespace TelegramApplication {
    public class TelegramBot {

        //InitData
        ITelegramBotClient bot;
        long idSystem = 771039930; //id-чат разговора между разработчиком и ботом
        //long idSystem;
        bool isStart;

        //ModesId
        //long realeseId = -1001427862218; //Настоящий id группы ИГРЫ (НЕ СТАЛИН)
        long debugId = -1001290847202; //id проверочной группы

        long idChat;
        public event EventHandler<SystemBotEventArgs> SystemMessage;

        public class SystemBotEventArgs : EventArgs {
            public string Description { get; set; }
            public Game Game { get; set; }
        }


        private Game game;

        public TelegramBot() { }
        public TelegramBot(string key) {
            bot = new TelegramBotClient(key);
        }

        public void Create() {
            idChat = debugId;
            //idChat = realeseId;


            bot.OnMessage += BotClient_OnMessage;
            bot.OnUpdate += Bot_OnUpdate;

            CreateGame();
            bot.StartReceiving();
        }

        PollAnswer pollAnswer;
        private void Bot_OnUpdate(object sender, Telegram.Bot.Args.UpdateEventArgs e) {
            pollAnswer =e.Update.PollAnswer;
            int r = 5;
        }

        int idPoll;

        private async void SendPoll(){

            Poll poll =new Poll();
            poll.Question ="Выбираем ближайшую игру:";
            

            await bot.SendPollAsync(
                chatId: idChat,
                question: "Выбираем ближайшую игру:",
                options: new[]{
                    "🎯 Дартс",
                    "🎲 Кубик",
                    "🏀 Мяч",
                    "🎳 Боулинг",
                    "🎰 Игровой автомат"},
                isAnonymous:false);
        }

        private async void CheckAndClosePoll(){
            //Poll  poll = await bot.StopPollAsync(
            //    idChat,
            //    idPoll);
        }

        private async void CreateGame() {
            game = new Game();
            game.GameChat = await bot.GetChatAsync(idChat);

            game.OnAction += Game_OnAction;
            game.OnMessage += Game_OnMessage;
            game.OnMistake += Game_OnMistake;
        }

        private void Game_OnMistake(object sender, MistakeMessage e) {
            DeleteMessage(e.Id);
        }
        public async void DeleteMessage(int id) {
            await bot.DeleteMessageAsync(chatId: game.GameChat.Id
                , messageId: id);
        }

        private void Game_OnMessage(object sender, ImportantMessage e) {

            switch (e.Status) {
                case "start":
                    SendImportantMessage(e.Text);
                    break;
                case "full":
                    CloseUserMessage(e.UserId);
                    SendGameMessage(e.Text);
                    break;
                case "over":
                    CloseChat(game.GameChat.Id);
                    SendImportantMessage(e.Text);
                    break;
                default:
                    SendGameMessage(e.Text);
                    break;

            }
        }

        private void Game_OnAction(object sender, ImportantMessage e) {
            if(e.Status == "start") {
                SendTitleMessage(e.Text);
            }
            SendActionMessage(e.Text);
            
        }

        #region TelegramSystem
        

        private ChatPermissions OpenPermissions() {
            ChatPermissions permissions = new ChatPermissions();
            permissions.CanSendMessages = true;
            permissions.CanSendOtherMessages = true;

            permissions.CanAddWebPagePreviews = false;
            permissions.CanChangeInfo = false;
            permissions.CanInviteUsers = true;
            permissions.CanPinMessages = false;
            permissions.CanSendMediaMessages = false;
            permissions.CanSendPolls = false;

            return permissions;
        }

        private ChatPermissions ClosePermissions() {
            ChatPermissions permissions = new ChatPermissions();

            permissions.CanAddWebPagePreviews = false;
            permissions.CanChangeInfo = false;
            permissions.CanInviteUsers = true;
            permissions.CanPinMessages = false;
            permissions.CanSendMediaMessages = false;
            permissions.CanSendMessages = false;
            permissions.CanSendOtherMessages = false;
            permissions.CanSendPolls = false;

            return permissions;
        }

        private void OpenChat(long id) {
            ChatPermissions permissions = OpenPermissions();
            bot.SetChatPermissionsAsync(id, permissions);
        }

        private void CloseChat(long id) {
            ChatPermissions permissions = ClosePermissions();
            bot.SetChatPermissionsAsync(id, permissions);
        }

        public void OpenUserMessage(int userId) {
            ChatPermissions permissions = OpenPermissions();
            bot.RestrictChatMemberAsync(game.GameChat.Id, userId, permissions);
        }

        public void CloseUserMessage(int userId) {
            ChatPermissions permissions = ClosePermissions();
            bot.RestrictChatMemberAsync(game.GameChat.Id, userId, permissions);
        }

        public async Task SendImportantMessage(string text) {
            await bot.SendTextMessageAsync(chatId: game.GameChat.Id
                , text: text
                , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html);
        }

        public async Task SendGameMessage(string text) {
            if(game.TitleId != 0) {

                await bot.SendTextMessageAsync(chatId: game.GameChat.Id
                    , text: text
                    , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html
                    , disableNotification: true
                    , replyToMessageId: game.TitleId);
            }
        }

        public async Task SendMessage(string title, long idChat) {
            await bot.SendTextMessageAsync(chatId: idChat
                , text: title
                , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html);
        }

        public async Task SendActionMessage(string text) {

            string action = game.Mode;
            ReplyKeyboardMarkup replyKeyboard = new ReplyKeyboardMarkup(new KeyboardButton(action));
            replyKeyboard.ResizeKeyboard = true;

            await bot.SendTextMessageAsync(chatId: game.GameChat.Id
                , text: text
                , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html
                , replyMarkup: replyKeyboard
                , disableNotification: true);
        }

        public async Task SendTitleMessage(string text) {
            string action = game.Mode;
            ReplyKeyboardMarkup replyKeyboard = new ReplyKeyboardMarkup(new KeyboardButton(action));
            replyKeyboard.ResizeKeyboard = true;

            game.TitleId = bot.SendTextMessageAsync(chatId: game.GameChat.Id
                , text: text
                , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html
                , replyMarkup: replyKeyboard
                , disableNotification: true).Result.MessageId;
        }
        #endregion

        #region TelegramActions
        private void BotClient_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e) {

            if(e.Message !=null ) {
                Message message = e.Message;
                long idChat = e.Message.Chat.Id;

                if (idChat == idSystem) {
                    Settings(message);
                }
                if(idChat == this.idChat && game.Status =="run") {
                    game.ExecuteAction(message);
                }
            }
        }


        private void Settings(Message message) {

            string sText = "Вы, пока не можете менять настройки турнира.";

            if (message.Chat.Id == idSystem) {
                if (isStart) {
                    string text;
                    if (message.Text == "/default") {
                        text = "Турнир до 20:35 (МСК)" +
                            "\n\nПриз: 50 рублей на Сбербанк" +
                           "\nСпонсор турнира https://t.me/iamnotstalin/";
                    } else {
                        text = message.Text;
                    }
                    CheckAndClosePoll();

                    game.Comment = text;
                    game.Launch();
                    OpenChat(idChat);
                    sText = "Игра запустилась.\n" +
                        "/finish Завершить";
                    isStart = false;
                } else {
                    switch (message.Text) {
                        case "/start":
                            isStart = true;

                            sText = $"Что записать в комментарий?\n\n/default";
                            break;
                        case "/finish":
                            if(game.Status == "run") {
                                game.Finish();
                            }
                            CreateGame();
                            CloseChat(idChat);
                            sText = "Игра завершилась.\n" +
                                "/start Запустить";
                            break;


                            case "/poll":
                                SendPoll();
                                break;
                        default:
                            sText = $"Какую команду выполнить в турнире?" +
                                $"\nСписок команд:" +
                                $"\n\n/start Запустить\n" +
                                $"\n/finish Остановить\n" +
                                $"\n/poll Опрос\n";


                            break;
                    }
                }
            }
            SendSystemMessage(message.From);
        }


        //Отправляю сообщения для настройки турнира

        public async Task SendSystemMessage(User user) {

            InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    new InlineKeyboardButton{
                        Text ="Запустить",
                        CallbackData = "LaunchGameCallback" }
                },

                new []{
                    new InlineKeyboardButton{
                        Text ="Остановить",
                        CallbackData ="StopGameCallback" },
                },
                    new []{
                        new InlineKeyboardButton{
                        Text ="Настройки",
                        CallbackData ="SettingsCallback" }
                    }
                });

            await bot.SendTextMessageAsync(
                chatId: user.Id,
                text: "Здравствуйте.\nКакую команду выполнить в турнире?",
                replyMarkup: keyboard
                );

            bot.OnCallbackQuery += Bot_OnCallbackQuery;
        }

        //Обрабатываем нажатия на кнопки настроек
        private void Bot_OnCallbackQuery(object sender, Telegram.Bot.Args.CallbackQueryEventArgs e) {

            CallbackQuery callbackQuery = e.CallbackQuery;
            switch (e.CallbackQuery.Data){

                //Страница управления системой (1-ая системная страница)
                case "LauncGameCallback":
                    StartGame();
                break;

                case "StopGameCallback":
                    StopGame();
                break;

                case "SettingsCallback":
                    GetGeneralSettingsPage(callbackQuery);
                break;


                //Страница общих настроек (2-ая системная страница)
                case "MainSettingsChangeCallback":
                    GetMainSettingsPage(callbackQuery);
                break;

                case "GameSettingsChangeCallback":
                    GetGameSettingsPage(callbackQuery);
                break;

                case "AdditionSettingsChangeCallback":
                break;


                //Страница основных настроек
                case "SetGameChannelCallback":

                break;

                case "SetGameChatCallback":

                break;

                //Страница игровых настроек
                case "SetGameTimeCallback":
                    GetTimeSettingsPage(callbackQuery);
                break;

                case "GameModeChangeCallback":

                break;


                //Страница установки времни турнира
                case "SetTimeStartGameCallback":
                break;

                case "SetTimeFinishCallback":
                break;



            }
        }

        //Начинаем игру
        private string StartGame() {
            if(isStart) {
                CheckAndClosePoll();

                game.Launch();
                OpenChat(idChat);
                isStart = false;
                return "Игра запустилась.";
            } else {
                return "Игра уже запущена.";
            }
        }

        //Останавливаем и завершаем игру
        private string StopGame() {
            if(game.Status == "run") {
                game.Finish();
            }

            CreateGame();
            CloseChat(idChat);
            return "Игра завершилась.";
        }

        //Отправить системную страницу общих настроек
        private async void GetGeneralSettingsPage(CallbackQuery callbackQuery) {
            InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    new InlineKeyboardButton{
                        Text ="Основные",
                        CallbackData = "MainSettingsChangeCallback" }
                },

                new []{
                    new InlineKeyboardButton{
                        Text ="Игровые",
                        CallbackData ="GameSettingsChangeCallback" },
                },
                    new []{
                        new InlineKeyboardButton{
                        Text ="Дополнительные",
                        CallbackData ="AdditionSettingsChangeCallback" }
                    }
                });

            await bot.EditMessageTextAsync(callbackQuery.Message.Chat.Id,
                callbackQuery.Message.MessageId,
                text: "Хорошо, продолжим.\nКакие настройки будем улучшать ?",
                replyMarkup: keyboard);
        }


        //Отправить страницу основных настроек
        private async void GetMainSettingsPage(CallbackQuery callbackQuery) {
            InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    new InlineKeyboardButton{
                        Text ="Установить канал для объявлений",
                        CallbackData ="SetGameChannelCallback" }
                },
                new []{
                    new InlineKeyboardButton{
                        Text ="Установить игровой чат",
                        CallbackData = "SetGameChatCallback"
                    }
                }  
            });

            await bot.EditMessageTextAsync(callbackQuery.Message.Chat.Id,
                callbackQuery.Message.MessageId,
                text: "Отлично, дальше.\nКакие настройки будем улучшать ?",
                replyMarkup: keyboard);
        }


        //Отправить страницу игровых настроек
        private async void GetGameSettingsPage(CallbackQuery callbackQuery) {
            InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    new InlineKeyboardButton{
                        Text ="Установить время турнира",
                        CallbackData ="SetGameTimeCallback" }
                },
                new []{
                    new InlineKeyboardButton{
                        Text ="Сменить режим игры",
                        CallbackData = "GameModeChangeCallback"
                    }
                }
            });

            await bot.EditMessageTextAsync(callbackQuery.Message.Chat.Id,
                callbackQuery.Message.MessageId,
                text: "Замечательно, продолжаем.\nКакие настройки будем улучшать?",
                replyMarkup: keyboard);
        }

        //Получить страницу настроек времени турнира
        private async void GetTimeSettingsPage(CallbackQuery callbackQuery) {
            InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    new InlineKeyboardButton{
                        Text ="Установить время начала",
                        CallbackData ="SetTimeStartGameCallback" }
                },
                new []{
                    new InlineKeyboardButton{
                        Text ="Установить время завершения",
                        CallbackData = "SetTimeFinishCallback"
                    }
                }
            });

            await bot.EditMessageTextAsync(callbackQuery.Message.Chat.Id,
                callbackQuery.Message.MessageId,
                text: "Почти закончили.\nКакие настройки будем улучшать?",
                replyMarkup: keyboard);
        }


        private async void GetModeGameSettingsPage(CallbackQuery callbackQuery) {
            InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    new InlineKeyboardButton{
                        Text ="Установить количество бросков",
                        CallbackData = "SetMaxThrowsCallback" }
                },

                new []{
                    new InlineKeyboardButton{
                        Text ="Число победителей",
                        CallbackData ="SetMaxWinnersCallback" },
                },
                    new []{
                        new InlineKeyboardButton{
                        Text ="Порядок победы",
                        CallbackData ="SetOrderOfVictoryCallback" }
                    }
                });

            await bot.EditMessageTextAsync(callbackQuery.Message.Chat.Id,
                callbackQuery.Message.MessageId,
                text: "Почти закончили.\nКакие настройки будем улучшать?",
                replyMarkup: keyboard);
        }


        //Отправить страницу игровых настроек
        private async void SetGameSettings(string command) {
            switch(command) {
                case "/StartTime":
                break;
            }

            ChatMember[] admins= await bot.GetChatAdministratorsAsync(game.GameChat.Username);
            List<ChatMember> administrators = admins.ToList();
        }

        #endregion

    }
}
